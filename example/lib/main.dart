import 'package:flutter/material.dart';
import 'package:mxh_location/mxh_location.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  String locationStr = "";

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('原生定位插件Demo'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(locationStr),
              FloatingActionButton(
                child: Text("开始定位"),
                onPressed: ()=> startLocation()
              ),
              FloatingActionButton(
                  child: Text("结束定位"),
                  onPressed: () async {
                    await MxhLocation.instance.stopLocation;
                    setState(() {
                      locationStr = "";
                    });
                  }
              ),
              FloatingActionButton(
                  child: Text("测试安卓"),
                  onPressed: () async {
                    String test = await MxhLocation.instance.testandroid();
                    setState(() {
                      locationStr = test;
                    });
                  }
              )
            ],
          ),
        ),
      ),
    );
  }

  void startLocation() async {
    //开启定位
    MxhLocation.instance.startLocation();
    //接收定位信息
    MxhLocation.instance.listen((event) {
      print("定位信息v：${event}");
    }, (error) {

    });
  }





}
