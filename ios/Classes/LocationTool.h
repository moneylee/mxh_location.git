//
//  LocationTool.h
//  mxh_location
//
//  Created by 李财 on 2022/2/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//block回调函数
typedef void(^currentLocationBlock)(NSString * latitude, NSString * longitude);

@interface LocationTool : NSObject

@property(nonatomic,copy) currentLocationBlock currentLocation;

- (void)startLocation;

- (void)stopLocation;

@end

NS_ASSUME_NONNULL_END
