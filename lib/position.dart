

class Position {
  final double? longitude;
  final double? latitude;
  Position(this.longitude, this.latitude);
}