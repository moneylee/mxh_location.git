import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mxh_location/mxh_location.dart';

void main() {
  const MethodChannel channel = MethodChannel('mxh_location');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await MxhLocation.platformVersion, '42');
  });
}
